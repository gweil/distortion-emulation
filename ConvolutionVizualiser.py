import numpy as np
import matplotlib.pyplot as plt
from Networks import CRAFx

if __name__ == "__main__":
    model = CRAFx(256)
    model.load_weights('training_weights.h5', by_name=True)

    convLayer = model.get_layer(name="conv1d")
    weights = np.array(convLayer.get_weights()[0])

    fig, axs = plt.subplots(8, 8, sharex=True, sharey=True)
    for xIndex in range(8):
        for yIndex in range(8):
            filterIndex = xIndex*8 + yIndex
            y = weights[filterIndex, 0]
            axs[xIndex, yIndex].set_ylim([-1,1])
            axs[xIndex, yIndex].plot(y)
    plt.show()
