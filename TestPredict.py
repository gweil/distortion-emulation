import os
import numpy as np
from scipy.io import wavfile

if ('SERVER_EXEC' in os.environ and os.environ["SERVER_EXEC"] == "True"):
    import matplotlib as mpl
    mpl.use('Agg')
    os.environ["CUDA_VISIBLE_DEVICES"] = '2'
else:
    import matplotlib.pyplot as plt

from Networks import CRAFx
from Dataset import Dataset

frameSize = 256

if __name__ == "__main__":
    in_audio = "../data/guitar_default.wav"
    tar_audio = "../data/guitar_gain_6.wav"
    dataset = Dataset(in_audio, tar_audio, duration=30, train_ratio=0.8, frame_size=frameSize, hop_size=frameSize//2)

    (xTrain, yTrain), (xTest, yTest) = dataset.formatTrainingDataset(kContext=8)

    model = CRAFx(frameSize=frameSize)
    model.load_weights('./training_weights.h5', by_name=True)

    y_ = np.array([])
    tmp = np.array([])
    raw = np.array([])
    hannWindow = np.hanning(frameSize)
    hannWindow = np.expand_dims(hannWindow, axis=1)
    hannWindow = np.expand_dims(hannWindow, axis=0)
    for step in range (yTest.shape[0]):
        print("Status: {}/{}".format(step, yTest.shape[0]))
        x_ = np.expand_dims(xTest[step], axis=0)

        if y_.shape[0] == 0:                            # First iteration
            y_ = model.predict(x_)
            y_[:,frameSize//2:,:] *= hannWindow[:,frameSize//2:,:]
            tmp = yTest[step]
            raw = xTest[step, -1]
            continue
        elif step == (yTest.shape[0] - 1):              # Last iteration
            if step%2 != 0: break
            pred = model.predict(x_)
            pred[:,:frameSize//2,:] *= hannWindow[:,:frameSize//2,:]
            y_[:,y_.shape[1]-frameSize//2:,:] += pred[:,:frameSize//2,:]
            y_ = np.concatenate((y_, pred[:,frameSize//2:,:]), axis=1)
        else:                                           # Intermediate iteration
            pred = model.predict(x_) * hannWindow
            y_[:,y_.shape[1]-frameSize//2:,:] += pred[:,:frameSize//2,:]
            y_ = np.concatenate((y_, pred[:,frameSize//2:,:]), axis=1)

        if step % 2 == 0:
            tmp = np.concatenate((tmp, yTest[step]), axis=0)
            raw = np.concatenate((raw, xTest[step, -1]), axis=0)

    yTest = tmp.reshape(tmp.shape[0]*tmp.shape[1], 1)
    y_ = y_.reshape(y_.shape[0]*y_.shape[1], 1)
    raw = raw.reshape(raw.shape[0]*raw.shape[1], 1)
    diff = yTest/abs(max(yTest.min(), yTest.max(), key=abs)) - y_/abs(max(y_.min(), y_.max(), key=abs))

    if not os.path.exists('./predictions'):
        os.makedirs('./predictions')

    x = np.arange(yTest.shape[0])
    fig, axs = plt.subplots(4, figsize=(20, 11))
    axs[0].plot(x, raw)
    axs[0].set_title('Raw signal')
    axs[1].plot(x, yTest)
    axs[1].set_title('Target signal')
    axs[2].plot(x, y_)
    axs[2].set_title('Predicted signal')
    axs[3].plot(x, diff)
    axs[3].set_title('Difference btw target and pred')
    plt.subplots_adjust(bottom=0.05, top=0.95, left=0.05, right=0.95, hspace=0.3)
    fig.savefig("./predictions/targetVSpred.png")
    plt.show()

    wavfile.write('./predictions/predictedSignal.wav', 44100, y_)
    wavfile.write('./predictions/targetSignal.wav', 44100, yTest)
    wavfile.write('./predictions/rawSignal.wav', 44100, raw)
