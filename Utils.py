import tensorflow as tf
from tensorflow.keras import backend as K

def unstackFunc(inputList):
    return tf.unstack(inputList, axis=1)

def booleanMask(x):
    outTensor = K.greater_equal(x[0], x[1])
    outTensor = K.cast(outTensor, dtype='float32')
    outTensor = tf.multiply(outTensor, x[1])
    return outTensor
