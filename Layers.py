import tensorflow as tf
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras import activations, initializers, regularizers, constraints
from tensorflow.keras.layers import Layer, Lambda, Conv2DTranspose, UpSampling1D, InputSpec
# from tensorflow.keras.engine import InputSpec
from tensorflow.python.keras.utils import conv_utils

class Conv1D_local(Layer):
    # Locally-connected 1D convolutional layer. Performs one-to-one convolutions to input feature map.
    # Taken from https://github.com/mchijmma/DL-AFx/blob/master/src/Layers.py
    # Original paper : Deep Learning for Black-Box Modeling of Audio Effects, 
    #                  Martinez Ramirez,
    #                  ( https://www.mdpi.com/2076-3417/10/2/638/pdf )
    def __init__(self, filters,
                 kernel_size,
                 strides=1,
                 padding='valid',
                 dilation_rate=1,
                 activation=None,
                 use_bias=True,
                 kernel_initializer='glorot_uniform',
                 bias_initializer='zeros',
                 kernel_regularizer=None,
                 bias_regularizer=None,
                 activity_regularizer=None,
                 kernel_constraint=None,
                 bias_constraint=None,
                 input_dim=None,
                 input_length=None,
                 **kwargs):

        if padding not in {'valid', 'same', 'causal'}:
            raise Exception('Invalid padding mode for Convolution1D:', padding)
        
        super(Conv1D_local, self).__init__(**kwargs)
        
        self.rank = 1
        self.filters = filters
        self.kernel_size = conv_utils.normalize_tuple(kernel_size, self.rank, 'kernel_size')
        self.strides = conv_utils.normalize_tuple(strides, self.rank, 'strides')
        self.padding = conv_utils.normalize_padding(padding)
        self.data_format = conv_utils.normalize_data_format('channels_last')
        self.dilation_rate = conv_utils.normalize_tuple(dilation_rate, self.rank, 'dilation_rate')
        self.activation = activations.get(activation)
        self.use_bias = use_bias
        self.kernel_initializer = initializers.get(kernel_initializer)
        self.bias_initializer = initializers.get(bias_initializer)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.bias_regularizer = regularizers.get(bias_regularizer)
        self.activity_regularizer = regularizers.get(activity_regularizer)
        self.kernel_constraint = constraints.get(kernel_constraint)
        self.bias_constraint = constraints.get(bias_constraint)
        self.input_spec = InputSpec(ndim=self.rank + 2)
        self.input_dim = input_dim
        self.input_length = input_length
        if self.input_dim:
            kwargs['input_shape'] = (self.input_length, self.input_dim)

    def build(self, input_shape):
        if self.data_format == 'channels_first':
            channel_axis = 1
        else:
            channel_axis = -1
        if input_shape[channel_axis] is None:
            raise ValueError('The channel dimension of the inputs '
                                'should be defined. Found `None`.')
        input_dim = input_shape[channel_axis]
        kernel_shape = self.kernel_size + (1, self.filters)

        self.kernel = self.add_weight(shape=kernel_shape,
                                        initializer=self.kernel_initializer,
                                        name='kernel',
                                        regularizer=self.kernel_regularizer,
                                        constraint=self.kernel_constraint)
        if self.use_bias:
            self.bias = self.add_weight(shape=(self.filters,),
                                        initializer=self.bias_initializer,
                                        name='bias',
                                        regularizer=self.bias_regularizer,
                                        constraint=self.bias_constraint)
        else:
            self.bias = None
        # Set input spec.
        self.input_spec = InputSpec(ndim=self.rank + 2,
                                    axes={channel_axis: input_dim})
        self.built = True

    def compute_output_shape(self, input_shape):
        if self.data_format == 'channels_last':
            space = input_shape[1:-1]
            new_space = []
            for i in range(len(space)):
                new_dim = conv_utils.conv_output_length(
                    space[i],
                    self.kernel_size[i],
                    padding=self.padding,
                    stride=self.strides[i],
                    dilation=self.dilation_rate[i])
                new_space.append(new_dim)
            return (input_shape[0],) + tuple(new_space) + (self.filters,)
        if self.data_format == 'channels_first':
            space = input_shape[2:]
            new_space = []
            for i in range(len(space)):
                new_dim = conv_utils.conv_output_length(
                    space[i],
                    self.kernel_size[i],
                    padding=self.padding,
                    stride=self.strides[i],
                    dilation=self.dilation_rate[i])
                new_space.append(new_dim)
        return (input_shape[0], input_shape[1], self.filters) 
    
    def call(self, inputs):
        x = tf.split(inputs, self.filters, axis = 2)
        W = tf.split(self.kernel, self.filters, axis = 2)
        outputs = []
        for i in range(self.filters):
            output = K.conv1d(x[i], W[i],
                              strides=self.strides[0],
                              padding=self.padding,
                              data_format=self.data_format,
                              dilation_rate=self.dilation_rate[0])
            outputs.append(output)
        outputs = K.concatenate(outputs,axis=-1)
        
        if self.use_bias:
            outputs = K.bias_add(
                outputs,
                self.bias,
                data_format=self.data_format)
        if self.activation is not None:
            return self.activation(outputs)
        return outputs

    def get_config(self):
        config = {
            'filters': self.filters,
            'kernel_size': self.kernel_size,
            'strides': self.strides,
            'padding': self.padding,
            'data_format': self.data_format,
            'dilation_rate': self.dilation_rate,
            'activation': activations.serialize(self.activation),
            'use_bias': self.use_bias,
            'kernel_initializer': initializers.serialize(self.kernel_initializer),
            'bias_initializer': initializers.serialize(self.bias_initializer),
            'kernel_regularizer': regularizers.serialize(self.kernel_regularizer),
            'bias_regularizer': regularizers.serialize(self.bias_regularizer),
            'activity_regularizer': regularizers.serialize(self.activity_regularizer),
            'kernel_constraint': constraints.serialize(self.kernel_constraint),
            'bias_constraint': constraints.serialize(self.bias_constraint)
        }
        base_config = super(Conv1D_local, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))  

class SAAF(Layer):
    '''[references]
    [1] ConvNets with Smooth Adaptive Activation Functions for Regression, Hou, Le
    and Samaras, Dimitris and Kurc, Tahsin M and Gao, Yi and Saltz, Joel H,
    Artificial Intelligence and Statistics, 2017
     '''
    def __init__(self,
                 break_points,
                 break_range = 0.2,
                 magnitude = 1.0,
                 order = 2,
                 tied_feamap = True,
                 kernel_initializer = 'random_normal',
                 kernel_regularizer = None,
                 kernel_constraint = None,
                 **kwargs):
        super(SAAF, self).__init__(**kwargs)
        self.break_range = break_range
        self.break_points = list(np.linspace(-self.break_range, self.break_range, break_points, dtype=np.float32))
        self.num_segs = int(len(self.break_points) / 2)
        self.magnitude = float(magnitude)
        self.order = order
        self.tied_feamap = tied_feamap
        self.kernel_initializer = initializers.get(kernel_initializer)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.kernel_constraint = constraints.get(kernel_constraint)

    def build(self, input_shape):
        if self.tied_feamap:
            kernel_dim = (self.num_segs + 1, input_shape[2])
        else:
            kernel_dim = (self.num_segs + 1,) + input_shape[2::]
        self.kernel = self.add_weight(shape=kernel_dim,
                                     name='kernel',
                                     initializer=self.kernel_initializer,
                                     regularizer=self.kernel_regularizer,
                                     constraint=self.kernel_constraint)
        self.built = True

    def basisf(self, x, s, e):
        cpstart = tf.cast(tf.less_equal(s, x), tf.float32)
        cpend = tf.cast(tf.greater(e, x), tf.float32)
        if self.order == 1:
            output = self.magnitude * (0 * (1 - cpstart) + (x - s) * cpstart * cpend + (e - s) * (1 - cpend))
        else:
            output = self.magnitude * (0 * (1 - cpstart) + 0.5 * (x - s)**2 * cpstart
                                     * cpend + ((e - s) * (x - e) + 0.5 * (e - s)**2) * (1 - cpend))
        return tf.cast(output, tf.float32)

    def call(self, x):
        output = tf.zeros_like(x)
        
        if self.tied_feamap:
            output += tf.multiply(x,self.kernel[-1])
        else:
            output += tf.multiply(x,self.kernel[-1])
        for seg in range(0, self.num_segs):
            if self.tied_feamap:
                output += tf.multiply(self.basisf(x, self.break_points[seg * 2], self.break_points[seg * 2 + 1]), 
                                      self.kernel[seg])
            else:
                output += tf.multiply(self.basisf(x, self.break_points[seg * 2], self.break_points[seg * 2 + 1]), 
                                      self.kernel[seg])
        return output

    def get_config(self):
        config = {
            'kernel_initializer': initializers.serialize(self.kernel_initializer),
            'kernel_regularizer': regularizers.serialize(self.kernel_regularizer),
            'kernel_constraint': constraints.serialize(self.kernel_constraint),
            'break_points': self.break_points,
            'magnitude': self.magnitude,
            'order': self.order,
            'tied_feamap': self.tied_feamap
        }
        base_config = super(SAAF, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))   
