import tensorflow as tf
import numpy as np

def preemphasis(tensor):
    coef=0.95
    shape = tensor.get_shape()

    # Important for compile step where the tensor's shape is None
    if shape[0] == None:
        return tensor
    else:
        return tf.concat([[tensor[0]], tensor[1:] - coef * tensor[:-1]], 0)

def esr_backend(target, pred):
    # pre-emphasis filter
    target = tf.map_fn(preemphasis, target)
    pred = tf.map_fn(preemphasis, pred)

    loss = tf.math.reduce_sum(tf.math.square(tf.math.abs(tf.math.subtract(target, pred))))
    energy = tf.math.reduce_sum(tf.square(tf.math.abs(target))) + 0.000001
    return tf.math.divide(loss, energy)

def dc_backend(target, pred):
    loss = tf.math.square(tf.math.abs(tf.math.reduce_mean(tf.math.subtract(target, pred))))
    energy = tf.math.reduce_mean(tf.math.square(tf.math.abs(target))) + 0.000001
    return tf.math.divide(loss, energy)

def custom_loss(y_actual, y_predicted):
    ESR = esr_backend(y_actual, y_predicted)
    DC = dc_backend(y_actual, y_predicted)
    return tf.math.add(ESR, DC)
