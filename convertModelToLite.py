import tensorflow as tf
from Networks import CRAFx

model = CRAFx(frameSize=256)
model.load_weights('training_weights.h5', by_name=True)
model.save('saved_model/CRAFx')

converter = tf.lite.TFLiteConverter.from_saved_model("saved_model/CRAFx")
tflite_model = converter.convert()
with tf.io.gfile.GFile('CRAFx.tflite', 'wb') as f:
    f.write(tflite_model)
