# Black-Box Emulation of Non-Linear Audio Effects

---

The code of this repository is working with TensorFlow v2.3.

The files __main.py__, __Dataset.py__, __Layers.py__, __Loss.py__, __Networks.py__ and __Utils.py__ are the files used for the training of the model.

## Tests scripts

- __convertModelToLite.py__ is meant to compile the model and to convert it to be usable with TFLite.
    
- __ConvolutionVizualiser.py__ is meant to plot the convolutions of the layer _conv1d_ that separates the signal in a filterbank architecture.
    
- __TestPredict.py__ is meant to be used after the training to run inferences on audio samples and to save the result to a .wav file. The script also plots the signals.

### Other

This project was inspired by the CRAFx model introduced by Marco Martínez in ***Deep Learning for Black-Box Modeling of Audio Effects*** ([website](https://mchijmma.github.io/DL-AFx/))

### License

GNU GPL-3.0
