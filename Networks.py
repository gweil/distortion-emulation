from tensorflow.keras import backend as K
from tensorflow.keras import Input, Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import TimeDistributed, Conv1D, Activation, MaxPool1D, LSTM, UpSampling1D, Multiply, Add, Concatenate, Dense, Lambda, GlobalAveragePooling1D, Reshape, Conv1DTranspose
from tensorflow.keras.utils import plot_model

from Layers import Conv1D_local, SAAF
from Utils import unstackFunc, booleanMask
from Loss import custom_loss

def seBlock(convBlock, ampRatio=16):
    nbFeatures = K.int_shape(convBlock)[-1]
    x = Activation(K.abs, name='abs_se')(convBlock)
    x = GlobalAveragePooling1D(name='globalAverage_se')(x)
    x = Reshape((1, nbFeatures), name='reshape_se')(x)
    x = Dense(nbFeatures * ampRatio, activation='relu', name='dense_0_se')(x)
    x = Dense(nbFeatures, activation='sigmoid', name='dense_1_se')(x)
    return Multiply(name='mult_se')([convBlock, x])

def CRAFx(frameSize=4096):
    nbFramesContext = 8
    presentFrame = -1
    nbFilters = 64
    kernelSize = 64

    ###
    # Adaptive Front-End
    ###
    inLayer = Input(shape=(nbFramesContext+1, frameSize, 1), name='input')
    conv1d = TimeDistributed(Conv1D(nbFilters, kernel_size=kernelSize, padding='same'), name='conv1d')(inLayer)
    absolute = TimeDistributed(Activation(K.abs), name='abs')(conv1d)
    localConv1d = TimeDistributed(Conv1D_local(nbFilters, kernel_size=kernelSize*2, padding='same'), name='conv1d_local')(absolute)
    softplus = TimeDistributed(Activation('softplus'), name='softplus')(localConv1d)
    maxPool = TimeDistributed(MaxPool1D(pool_size=frameSize//64, padding='same'), name='maxpooling')(softplus)
    unstack = Lambda(unstackFunc, name='unstack_0')(maxPool)
    conc = Concatenate(name='concatenate')(unstack)

    ###
    # Latent space
    ###
    latentIn = LSTM(units=nbFilters*4, recurrent_dropout=0.1, dropout=0.1,
                    return_sequences=True, name='lstm_0')(conc)
    latent = LSTM(units=nbFilters*2, recurrent_dropout=0.1, dropout=0.1,
                  return_sequences=True, name='lstm_1')(latentIn)
    latent = LSTM(units=nbFilters, activation=None, recurrent_dropout=0.1,
                  dropout=0.1, return_sequences=True, name='lstm_2')(latent)
    latentOut = SAAF(break_points=25, break_range=0.2, magnitude=100, order=2,
                     tied_feamap=True, kernel_initializer='random_normal',
                     name='saaf_latent')(latent)

    ###
    # Synthesis Back-end
    ###
    unpool = UpSampling1D(frameSize//64, name='upsampling1d')(latentOut)
    R = Lambda(unstackFunc, name='unstack_1')(conv1d)
    mult = Multiply(name='mult')([unpool, R[presentFrame]])

    # DNN-SAAF-SE
    dnn = Dense(nbFilters, activation='tanh', name='dense_0')(mult)
    dnn = Dense(nbFilters//2, activation='tanh', name='dense_1')(dnn)
    dnn = Dense(nbFilters//2, activation='tanh', name='dense_2')(dnn)
    dnn = Dense(nbFilters, activation=None, name='dense_3')(dnn)
    dnnSAAF = SAAF(break_points=25, break_range=0.2, magnitude=100, order=2,
                   tied_feamap=True, kernel_initializer='random_normal',
                   name='saaf_dnn')(dnn)
    dnnSAAF_SE = seBlock(dnnSAAF)

    add = Add(name='add')([dnnSAAF_SE, mult])
    outLayer = Conv1DTranspose(filters=1, kernel_size=kernelSize, padding='same', activation=None, name='deconv')(add)

    ### Model
    model = Model(inputs=[inLayer], outputs=[outLayer])
    model.compile(loss=custom_loss, optimizer=Adam(lr=1e-4))
    return model

def Pretraining(frameSize=4096):
    nbFilters = 64

    inLayer = Input(shape=(frameSize, 1), name='input')
    conv1d = Conv1D(nbFilters, kernel_size=64, padding='same', name='conv1d')(inLayer)
    absolute = Activation(K.abs, name='abs')(conv1d)
    localConv1d = Conv1D_local(nbFilters, kernel_size=128, padding='same', name='conv1d_local')(absolute)
    softplus = Activation('softplus', name='softplus')(localConv1d)
    maxPool = MaxPool1D(pool_size=frameSize//64, padding='same', name='maxpooling')(softplus)
    up = UpSampling1D(size=frameSize//64, name='upsampling1d')(maxPool)
    maxValue = Lambda((booleanMask), name='boolean')([softplus, up])
    Y = Multiply(name='mult')([conv1d, maxValue])
    outLayer = Conv1DTranspose(filters=1, kernel_size=64, padding='same', activation=None, name='deconv')(Y)

    model = Model(name='pretraining_model', inputs=[inLayer], outputs=[outLayer])
    model.compile(loss=custom_loss, optimizer=Adam(lr=1e-4))

    return model

