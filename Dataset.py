import numpy as np
from scipy.io import wavfile

class Dataset():
    debug = False

    def __init__(self, wavfile_default, wavfile_target, frame_size=4096, hop_size=2048, duration = 'full', train_ratio=0.8):
        self.data_default = []
        self.data_target = []
        self.wavfile_default = wavfile_default
        self.wavfile_target = wavfile_target
        self.frame_size = frame_size
        self.hop_size = hop_size
        self.duration = duration
        self.train_ratio = train_ratio
        self.__loadWav()

    # Return a formated numpy array (nb_frame, frame_size, 1)
    def formatPretrainingDataset(self):
        default = self.data_default
        target = self.data_target

        # Convert to 1D numpy array
        default = np.stack(default, axis=0).flatten()
        target = np.stack(target, axis=0).flatten()
        maxDefault = abs(max(default.min(), default.max(), key=abs))
        maxTarget = abs(max(target.min(), target.max(), key=abs))
        default = default / maxDefault
        target = target / maxTarget

        # Cut data in 2 parts: training and test
        default_train, default_test = self.__cutData(default)
        target_train, target_test = self.__cutData(target)

        # Reshape data according to frame_size (nb_frames, frame_size, 1)
        xTrain = self.__reshape(default_train)
        yTrain = self.__reshape(target_train)
        xTest = self.__reshape(default_test)
        yTest = self.__reshape(target_test)

        return (xTrain, yTrain), (xTest, yTest)

    # Return a formated numpy array (nb_batch, kContext*2+1, frame_size, 1)
    def formatTrainingDataset(self, kContext=4):
        default = self.data_default
        target = self.data_target

        # Convert to 1D numpy array
        default = np.stack(default, axis=0).flatten()
        target = np.stack(target, axis=0).flatten()
        maxDefault = abs(max(default.min(), default.max(), key=abs))
        maxTarget = abs(max(target.min(), target.max(), key=abs))
        default = default / maxDefault
        target = target / maxTarget

        # Cut data in 2 parts: training and test
        default_train, default_test = self.__cutData(default)
        target_train, target_test = self.__cutData(target)

        # Reshape data according to frame_size
        default_train = self.__reshape(default_train)
        default_test = self.__reshape(default_test)
        target_train = self.__reshape(target_train)
        target_test = self.__reshape(target_test)

        xTrain = []
        for i in range (kContext, len(default_train), 1):
            xTrain.append(default_train[i-kContext:i+1])
        xTrain = np.array(xTrain)
        yTrain = target_train[kContext:]

        xTest = []
        for i in range (kContext, len(default_test), 1):
            xTest.append(default_test[i-kContext:i+1])
        xTest = np.array(xTest)
        yTest = target_test[kContext:]

        return (xTrain, yTrain), (xTest, yTest)

    def __cutData(self, x):
        assert self.train_ratio <= 1
        train_lim = int(self.train_ratio * len(x))

        # Instead of adding zero we drop the last frame if it is not full
        train = np.array([])
        for i in range (0, train_lim, self.hop_size):
            if train_lim - i >= self.frame_size:
                train = np.append(train, x[i:i+self.frame_size])
            else:
                break

        test = np.array([])
        for i in range (train_lim, len(x), self.hop_size):
            if len(x) - i >= self.frame_size:
                test = np.append(test, x[i:i+self.frame_size])
            else:
                break

        return train, test

    def __reshape(self, x):
        x = x.reshape( (int(len(x) / self.frame_size), self.frame_size, 1) )
        return x

    def __loadWav(self):
        ### Default
        self.sample_rate, self.data_default = wavfile.read(self.wavfile_default)
        ### Target
        _, self.data_target = wavfile.read(self.wavfile_target)

        if self.duration != 'full':
            duration = int(self.duration)
            self.data_default = self.data_default[0:self.sample_rate * duration]
            self.data_target = self.data_target[0:self.sample_rate * duration]
            assert len(self.data_default) / self.sample_rate == duration
            assert len(self.data_target) / self.sample_rate == duration

        self.data_default = np.expand_dims(self.data_default, 1)
        self.data_target = np.expand_dims(self.data_target, 1)
