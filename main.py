import os
import sys
import pickle
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import callbacks as clb

from Networks import Pretraining, CRAFx, CRAFx_realtime
from Dataset import Dataset

if __name__ == "__main__":
    print("TensorFlow version :", tf.__version__)
    print("Keras version :", keras.__version__)

    # Load dataset
    in_audio = "../data/guitar_default.wav"
    tar_audio = "../data/guitar_gain_6.wav"
    dataset = Dataset(in_audio, tar_audio, frame_size=256, hop_size=128, duration=1)
    batch_size = 40
    nb_epochs = 1
    if ('SERVER_EXEC' in os.environ and os.environ["SERVER_EXEC"] == "True"):
        print("SERVER_EXEC....\nLOADING FULL DATASET....")
        os.environ["CUDA_VISIBLE_DEVICES"] = '2'
        dataset = Dataset(in_audio, tar_audio, frame_size=256, hop_size=128, duration='full')
        batch_size = 500
        nb_epochs = 2000

    # Pretraining
    if len(sys.argv) == 1 or not '--no-pretraining' in str(sys.argv):
        (xTrain, yTrain), (xTest, yTest) = dataset.formatPretrainingDataset()
        dataPretraining = np.concatenate((xTrain, yTrain, xTest, yTest))
        print("Pretraining dataset loaded !")


        earlyStop = clb.EarlyStopping(monitor='loss',
                                      patience=75,
                                      verbose=1)
        chkpt = clb.ModelCheckpoint(filepath='pretraining_weights.h5',
                                    monitor='val_loss',
                                    verbose=1,
                                    save_weights_only=True,
                                    save_best_only=True)
        csvLog = clb.CSVLogger('./logs/pretraining.csv',
                               separator=';',
                               append=False)

        model = Pretraining(frameSize=256)
        history = model.fit(dataPretraining,
                            dataPretraining,
                            batch_size=batch_size,
                            epochs=nb_epochs,
                            validation_split=0.2,
                            callbacks=[earlyStop, chkpt, csvLog])

    # Training
    if not os.path.exists('dataset.pickle'):
        (xTrain, yTrain), (xTest, yTest) = dataset.formatTrainingDataset(kContext=8)
        with open("dataset.pickle", "wb") as f:
            pickle.dump(xTrain, f)
            pickle.dump(yTrain, f)
            pickle.dump(xTest, f)
            pickle.dump(yTest, f)
    else:
        with open("dataset.pickle", "rb") as f:
            xTrain = pickle.load(f)
            yTrain = pickle.load(f)
            xTest = pickle.load(f)
            ytest = pickle.load(f)

    reduce_lr = clb.ReduceLROnPlateau(monitor='val_loss',
                                      factor=0.25,
                                      verbose=1,
                                      patience=25)
    earlyStop = clb.EarlyStopping(monitor='val_loss',
                                  patience=75,
                                  verbose=1)
    chkpt = clb.ModelCheckpoint(filepath='training_weights.h5',
                                monitor='val_loss',
                                verbose=1,
                                save_weights_only=True,
                                save_best_only=True)
    csvLog = clb.CSVLogger('./logs/training.csv',
                           separator=';',
                           append=False)
    model = CRAFx(frameSize=256)
    model.load_weights('pretraining_weights.h5', by_name=True)
    if '--load-training' in str(sys.argv):
        model.load_weights('training_weights.h5', by_name=True)
    history = model.fit(xTrain,
                        yTrain,
                        batch_size=batch_size,
                        epochs=nb_epochs,
                        validation_split=0.2,
                        callbacks=[reduce_lr, earlyStop, chkpt, csvLog])
